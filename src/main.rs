use cargo_lock::Lockfile;

fn main() {
    let lockfile = Lockfile::load("Cargo.lock").unwrap();

    println!("CRATES=\"");

    for pkg in lockfile.packages {
        println!("\t{}-{}", pkg.name, pkg.version);
    }

    println!("\t\"");
    println!("inherit cargo");
}
