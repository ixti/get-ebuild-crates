# get-ebuild-crates

The purpose of this utility is to generate the contents of the CRATES variable
which is used in Gentoo ebuilds for rust packages that use crates.

Heavily inspired by [get-ego-vendor][1]

## Usage

``` bash
cd /path/to/foo
get-ebuild-crates > crates.txt
```

Then insert crates.txt contents to your ebuild (after EAPI=X).

[1]: https://github.com/williamh/get-ego-vendor
